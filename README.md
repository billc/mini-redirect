# MiniRedirect
An Elixir Plug web application to service i.e. redirect links shorten by the Mini project.

Features
  * Creates a new plug for redirecting requests based on the path passed in the http://mi.ni url.
  * Runs a separate independent web service.
  
Requirements
  * Mini project
  * Update to etc/hosts to allow mi.ni host to be bypassed to localhost
  * Nginx if you wish to port forward from localhost:80 to miniredirect running on 8080
  * Postgresql
  

DISCLAIMER
This is an experimental project and not intended for purposes outside of educational, non-professional needs. Source code is available under the MIT licenses with no warranty or liability.

defmodule MiniRepo.Link do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias MiniRedirect.Repo

  schema "links" do
    field :address, :string
    field :location, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:address, :location])
    |> validate_required([:address, :location])
  end

  def by_address(address) do
    query = from l in "links",
      where: l.address == ^address,
      select: l.location,
      limit: 1


    Repo.all(query)
  end

  def by_path(path) do
    query = from l in "links",
      where: l.path == ^path,
      select: l.location,
      limit: 1


    Repo.all(query)
  end
end

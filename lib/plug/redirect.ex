defmodule MiniRedirect.Plug.Redirect do
  require Logger

  import Plug.Conn

  alias MiniRepo.Link

  def init(options), do: options

  def call(%Plug.Conn{request_path: path} = conn, _options) do
    location = path
    |> Link.by_path
    do_redirect(conn, location)
  end

  defp do_redirect(conn, []), do: conn

  defp do_redirect(conn, [location | _]) do
    Logger.debug "Redirecting to #{location}"

    conn
    |> put_resp_header("location", location)
    |> send_resp(302, "")
    |> halt
  end
end

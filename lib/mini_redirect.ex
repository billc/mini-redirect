defmodule MiniRedirect do
  @moduledoc """
  Documentation for MiniRedirect.
  """
  use Application
  require Logger

  def start(_type, _args) do
    port = Application.get_env(:mini_redirect, :cowboy_port, 8080)

    import Supervisor.Spec, warn: false

    children = [
      Plug.Adapters.Cowboy.child_spec(:http, MiniRedirect.Router, [], port: port),
      supervisor(MiniRedirect.Repo, [])
    ]

    Logger.info "Started Application"
    opts = [strategy: :one_for_one, name: MiniRedirect.Supervisor]
    Supervisor.start_link(children, opts)
  end

end
